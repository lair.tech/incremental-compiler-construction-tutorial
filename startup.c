#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdbool.h>


/* define all scheme constatns */
#define empty_list   0x3F
#define bool_f       0x2F
#define bool_t       0x6F
#define fx_mask      0x03
#define fx_tag       0x00
#define fx_shift        2
#define char_mask    0xFF
#define char_tag     0x0F
#define char_shift      8
#define object_mask  0x07
#define pair_tag     0x01
#define vector_tag   0x05
#define string_tag   0x06
#define word_size       4

/* all scheme values are of type ptr(s) */
typedef unsigned int ptr;

typedef struct {
    ptr car;
    ptr cdr;
} pair;

static ptr pair_car(ptr x) {
    return ((pair*)(x - pair_tag))->car;
}

static ptr pair_cdr(ptr x) {
    return ((pair*)(x - pair_tag))->cdr;
}

static bool is_pair(ptr x) {
    return (x & object_mask) == pair_tag;
}

static void print_ptr_line(ptr x);

static void print_list_content(ptr x) {
    if (!is_pair(x)) {
        print_ptr_line(x);
        return;
    }

    // car printing
    ptr car = pair_car(x);
    print_ptr_line(car);

    // cdr printing
    ptr cdr = pair_cdr(x);

    if (cdr == empty_list) {
        return;
    }

    printf(" ");

    if (!is_pair(cdr)) {
        printf(". ");
        fflush(stdout);
    }

    print_list_content(cdr);
}

typedef struct {
    ptr  length;
    ptr  base_element;
} vector;

typedef struct {
    ptr  length;
    char base_char;
} string;

static void print_ptr_line(ptr x) {
    if ((x & fx_mask) == fx_tag) {
        printf("%d", ((int) x) >> fx_shift);
    } else if (x == bool_t) {
        printf("#t");
    } else if (x == bool_f) {
        printf("#f");
    } else if (x == empty_list) {
        printf("()");
    } else if ((x & char_mask) == char_tag) {
        char temp_char = ((int) x) >> char_shift;
        if (temp_char == '\t') {
            printf("#\\tab");
        } else if (temp_char == '\n') {
            printf("#\\newline");
        } else if (temp_char == '\r') {
            printf("#\\return");
        } else if (temp_char == ' ') {
            printf("#\\space");
        } else {
            printf("#\\%c", ((int) x) >> char_shift);
        }
    } else if (is_pair(x)) {
        printf("(");
        print_list_content(x);
        printf(")");
    } else if ((x & object_mask) == vector_tag) {
        int length    = ((int) ((vector*) (x - vector_tag))->length) >> fx_shift;
        ptr* elements = &((vector*) (x - vector_tag))->base_element;
        printf("#(");
        for(int i = 0; i < length; i++) {
            print_ptr_line(*(elements + i));
            if (i + 1 != length) {
                printf(" ");
            }
        }
        printf(")");
    } else if ((x & object_mask) == string_tag) {
        int   length = ((int) ((string*) (x - string_tag))->length) >> fx_shift;
        char* chars  = &((string*) (x - string_tag))->base_char;
        printf("\"");
        for(int i = 0; i < length; i++) {
            char c = *(chars + i);
            if (c == '\"') {
                printf("\\%c", c);
            } else if ( c == '\\') {
                printf("\\\\");
            } else {
                printf("%c", c);
            }
        }
        printf("\"");
    } else {
        printf("error printing: %d", x);
    }
}

static void print_ptr(ptr x) {
    print_ptr_line(x);
    printf("\n");
}

static char* allocate_protected_space(int size) {
    int page = getpagesize();
    int status;
    int aligned_size = ((size + page -1) / page) * page;
    char* p = mmap(0, aligned_size + 2 * page,
                   PROT_READ | PROT_WRITE,
                   MAP_ANONYMOUS | MAP_PRIVATE,
                   0, 0);
    if (p == MAP_FAILED) {
        printf("Failed to memory map the procted space");
    }
    status = mprotect(p, page, PROT_NONE);
    if (status != 0) {
        printf("Failed to memory protect the lower bound of the space");
    }
    status = mprotect(p + page + aligned_size, page, PROT_NONE);
    if (status != 0) {
        printf("Failed to memory protect the higher bound of the space");
    }
    return (p+page);
}

static void deallocate_protected_space(char* p, int size) {
    int page = getpagesize();
    int status;
    int aligned_size = ((size + page -1) / page) * page;
    status = munmap(p - page, aligned_size + 2 * page);
    if (status != 0) {
        printf("failed to deallocate memory protected space");
    }
}

typedef struct {
    void* eax;  /* 0   scratch */
    void* ebx;  /* 4   preserve */
    void* ecx;  /* 8   scratch */
    void* edx;  /* 12  scratch */
    void* esi;  /* 16  preserve */
    void* edi;  /* 20  preserve */
    void* ebp;  /* 24  preserve */
    void* esp;  /* 28  preserve */
} context;

int scheme_entry(context* ctxt, char* stack_base, char* heap);

int main(int argc, char** argv) {
    // allocate stack space
    int stack_size = 16 * 4096; // 16k cells
    char* stack_top = allocate_protected_space(stack_size);
    char* stack_base = stack_top + stack_size;
    // allocate heap space
    int heap_size = 16 * 1024 * 4096; // ~ 16 MB heap size
    char* heap = allocate_protected_space(heap_size);

    context ctxt;
    print_ptr(scheme_entry(&ctxt, stack_base, heap));

    // deallocate stack
    deallocate_protected_space(stack_top, stack_size);
    // deallocate heap
    deallocate_protected_space(heap, heap_size);

    return 0;
}
