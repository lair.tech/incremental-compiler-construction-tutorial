# Incremental Compiler Construction Tutorial 

This paper `Compilers: Backend to Frontend and Back to Front Again`(doc/tutorial.pdf) is an extended tutorial for the `An Incremental Approch to Compiler Construction`paper. It implements a basic compiler with the follwing features:
* lexicaly scoped scheme like language
* immediates
  * integers
  * booleans
  * empty list
  * chars
* compound values
  * pairs (lists)
  * vectors
  * strings
  
This repository implements all chapters form this tutorial. 

### Software needed:
* gcc
* chez scheme (https://github.com/cisco/ChezScheme.git)

#### Installing gcc and build tools:
* Arch/Manjaro: `sudo pacman -Syu base-devel`

##### Downloading ChezScheme source and build it:
* `git clone https://github.com/cisco/ChezScheme.git`
* `cd ChezScheme`
* `.configure`
* `sudo make install`

### Commit structure
Each commit will have a prefix based on the chapter. For example, chapter `1.1 Integers` will have the prefix `01 - <commit message>`

# DEV Log
## 2021-04-17 
### 1.1 - Integers
In the tutorial the names are not the same as in the supplied `tests-driver.scm` stuff. So `emit-program` should be `compile-program`, `test-driver.scm` -> `tests-driver.scm` etc.

Also the tutorial is a bit old, in the meantime gcc added directifies to there assembler output. That can be omitted with the `fno-asynchronous-unwind-tables` for example:
``` sh
gcc --omit-frame-pointer -fno-asynchronous-unwind-tables -S ctest.c
```

To get rid of the waring about the forward reference i also had to add `int scheme_entry();` in the `startup.c`.

Other then that it was strait forward to implement (copy).

### 1.2 - Immediate Constants
Just copied the tag example code into `startup.c` and `compiler.scm` but only support the integer stuff for now and make sure that the old tests still runs. At first i forgot to update the `startup.c` so only the `0` test was successful because the other integers are shifted 2 bits to the left. Fixed that and voila it runs fine.

After that i added the `boolean` support which was also pretty strait forward. Just convert the booleans `#t` -> `bool-t` = `#x2F` and `#f` -> `bool-f` = `#6F` in the `compiler.scm`. In the runtime `startup.c` just go the other way around with `x == bool_t` and `x == bool_f`, print `#t` or `#f` accordingly.

Then comes the empty list `()` with the same logic as the `boolean` but only with the tag `#x3F`.

Fixed a formatting bug in the `tests/tests-driver.scm` next. Didn't notice the false formatting before ;)

And the last immediate the char is also pretty strait forward. We convert the scheme char `x` by `(+ (ash (char->integer x) char-shift) char-tag)`. The `char-shift = 8` and the `char-tag = #0F`. The code to print the chars is pretty much the same as for the fixnums but there are 4 special cases because of the other repsentation in `scheme` compared to `c`:
* `\t` -> `#\\tab`
* `\n` -> `#\\newline`
* `\r` -> `#\\return`
* ` `  -> `#\\space`

### 1.3 - Unary Primitives
At first i refactored the `compiler.scm` for support of `immediate`s and `primcall`s and just tested if it still runs the old test. We are golden so far.

Added the primitive `fxadd1` which was basically a copy and paste of the code from the `tutorial.pdf`. Also added the `fixnum?` primitive whcih was also just a copy of the code from the `tutorial.pdf`, only needed to add `fx-tag` and `bool-bit`. By the `bool-bit` i was thinking wrong about how much to shift it to the left. The `bool-bit` is the 7th bit and i first shifted also 7 bits with of course shift it 1 to much. Added some error printing to the `startup.c` to figure that out. Now it's on `bool-bit = 6` and every thing works as intended. Also moved the `fixnum?` tests up in the `tests-1.3-req.scm` test file so it is after the `fxadd1` tests.

Added the rest of the primitives:
* `fixnum->char`: now i know why the tests was before the `fixnum?` it's first mention in the `tutorial`. Just came back from a break and skipped it.
* `char->fixnum`: just `shrl` 6 bits and be done with it 
* `fixnum?`: extracted the boolean comparsion stuff into it's own helper function `emit-bool-compare` that can be reused for all the tag comparsion stuff
* `fxzero?`: just `(emit-bool-compare 0)` with the filled arg in `eax`
* `null?`: same as `fxzero?` but with `empty-list` tag
* `boolean?`: was the most complicated from the newly implented ones because we can't just use the `bool-f` tag. Then the list would match to. So we needed a new mask `bool-mask` that matches all 6 lower bits. So we can extract the tag pattern `00101111` and compare it to `bool-f`. We can ignore the true bit for that.
* `char?`: same as `boolean` but with a `char-mask`, the hole 8 bits. And of course we compare it with the `char-tag`
* `not`: just `emit-bool-compare` with `bool-f` on eax
* `fxlognot`: just shift right 2 (`fx-shift`) to get the orignal number. Then not it and shift left 2 (`fx-shift`)

Other then that i also added some comments to the shifts, masks and tags definitions.

### 1.4 - Conditional Expressions
Added support for `if` special form. Basically just copy the code from the `tutorial.pdf` and added the missing helper functions.

Added support for `and` special form. Normally it would be a macro in a lisp but because we don't have macro support for it so it's a special form.

Also added support for the `or` special form. It's a bit of a hack for now because we don't have `let` bindings until chapter 1.6. So we just evaluate the parameter again for the return value of the `or` which will work for our purpely functional language for now. That case will be triggered if the or has more then 1 parameter.

## 2021-04-18 
### 1.4 - Conditional Expression
Tried to implement a Beep Hole Optimizer that runs over the generated assembly file and extract the unnessary comparisons. But had problems with the `srfi-115` for chez scheme because it didn't support `neg-look-ahead` so i could not easly exclude the `not` predicate comparsion with `cmp $47, %eax` from the optimization. It have the same signature as the other comparsions and also have a `boolean` value generation that is actually need later on. Otherwise the result is not correct.

For example the `(if (not (boolean? #t)) 15 (boolean? #f))` replaced the not with just a comparsion with the end result of `15` instead of `#t`.

Could have make it work with some more code but it was pretty ugly already because of the regex match and working around that would not have made it prettier. Other options i was thinking about:
* Don't just emit assembly into a file but build up assembly s-expressions and let the beep hole optimizer work on that (preferable with pattern matching)
  * To much work and for each new used instruction later on an appropiate s-expression must be added. But still better then read out the file and regex match against it like i tried ;)
* Introduce primitive predicate distinction and keep that info around. When we emit an expression from a `if-test` then only emit the core logic without the boolean value generation. Except for the `not` function.
  * Also to much work for what we get in the end and also complicates the compiler unessary
* Introduce some immediate language and multiple passes that allow easy dataflow and other optimizations like cps (continuation passing style), ssa (single static assigment) or anf (a-normal form)
  * Way to much work compared to the simple 1 pass compiler that we are developing here
  
In the end i just added some comments in the form `# begin: <expr>` and `# end: <expr>` to the generated assembly file. At first for debugging my regex matcher, sadly that also complicated the regex matcher further. But in the end i'm fine with the added readability of the generated assembly file.

Would be glad if someone could give me a hint of some simple options i missed. I mean the hole `tutorial` is pretty easy so far, that challenge is a bit off. Most likely missed a super simple solution that is quite obvious ;)

Also had to play around with [akku](https://akkuscm.org/) (checkout + build) to actually get the `srfi-115` regex matching libary. And added the following to my `.chez-geiser` file so `geiser` actually knows about the libary path:

``` emacs-lisp
;; © 2019 Göran Weinholt. SPDX-License-Identifier: MIT.
(let ()
  ;; This code has the same effect as .akku/lib/activate, but runs
  ;; directly in Chez Scheme.
  (define dirsep (string (directory-separator)))
  (define (find-akku-root)
    (let lp ((dir (cd)))
      (let ((libdir (string-append dir dirsep ".akku" dirsep "lib")))
        (if (file-directory? libdir)
            dir
            (let ((parent (path-parent dir)))
              (if (equal? parent (path-root dir))
                  #f
                  (lp parent)))))))
  (define (set-ld-library-path variable root)
    (putenv variable
            (string-append root dirsep ".akku" dirsep "ffi"
                           (cond ((getenv variable) =>
                                  (lambda (dir) (string-append ":" dir)))
                                 (else "")))))
  (cond ((find-akku-root) =>
         (lambda (root)
           (printf "~%Using ~s as the Akku project root~%" root)
           ;; The search directory for Scheme source and compiled objects
           (library-directories
            (list (cons (string-append root dirsep ".akku" dirsep "lib")
                        (string-append root dirsep ".akku" dirsep "libobj"))))
           ;; The search directory for the FFI
           (set-ld-library-path "LD_LIBRARY_PATH" root)
           (set-ld-library-path "DYLD_LIBRARY_PATH" root)))
        (else
         (printf "~%No .akku/lib found in or above ~s~%" (cd)))))
```

If the project need some dependencies in the future i will add them to the akku files and commit them. For now they are just useless again.

### 1.5 - Binary Primitives
Copied the stack allocation from the `tutorial` into the `startup.c` and did the same for the `compiler.scm` changes. Worked nearly out of the box only had to extend the `gcc` call with the `-m32` option so it creates a `32-bit` executeable and not a `64-bit` one. Otherwise we get segfaults because of the false stack alignment.

Did a bit of refactoring on the primitive unary/binary functions so that they reuse more code. Also moved the hole primitive function stuff into it's own file `primitives.scm` so the `compiler.scm` won't get so to overloaded because of the dozen of repetitive primitive functions.

The refactoring for the binary comparsion primitives was really nice, they are basically all a one liner now. And the `char{comparator}` and `fx{comparator}` are the same except the name. Was thinking about to provide the according `prim{comparator}` functions and just map the others to it. But sadly `(define fx{comparator} prim{comparator})` is not possible because it also need to be added to the 3 props `*is-prim*`, `*arg-count*` and `*emitter*`. Maybe i will do that later on but doubt it ;)

Also implemented the optimization for the binary calls if one of the arguments is a immediate and it is communtitive primitive. If the first argument is a immediate we can keep the evaluation order from left to right otherwise we flip the arguments. So that the non-immeditate is always evaluated first. Of course that don't work for non-communtitive primitives like `-` (`fx-`) and we need to place the first argument onto the stack. The comparsion operators are not communtitive except the `=` (`sete`) but we can still flip the arguments if we also flip the comparsion operator like:
* `<`  (`setl`)  <-> `>`  (`setg`)
* `<=` (`setle`) <-> `>=` (`setge`) 

The code is now more consistent and reuses much more functions but not sure if the introduced indirections make the code more readable. Also the optimization about the immediate values for binary primitivies makes it harder to read and understand the code. The generated assembly is now also not so regular and one must pay closer attention about the actual passed arguments. Not sure that this is a good trade of for a compiler that won't get really fast anyways because it just generate code on the go. From my point of view, it's only worth to pursue the optimizations because of the learning experience and some parts may be useful in the future but for the compiler itself, i would skip them. If it's purpose is a clear and simple compiler.

## 2021-04-19
### 1.6 Local Variables
The implemention of the `let` bindings was pretty strait forward. Just copied the code and modified all places accordingly. Also made some changes to the signatures of the primitive emit functions so it's more in line with the rest. Meaning the stack index (`si`) is now the first argument, then the enviroment (`env`) followed by the rest of the parameters.

Also fixed the `or` special form with the newly introducted `let` binding so it will only evaluate all arguments at most once.

## 2021-04-20
### 1.6 Local Variables
Implemented the `let*` special form by transforming the `let*` to a nested `let` form. Normaly that would be done by a (syntax) marco in the language itself but we have none for now. So there is a simple transformer function that getts the bindings for the `let*` from and take each binding and create a `let`form with quasi quotation and calls itself recursively with the rest of the bindings until no bindings are left. Then it insert the let-body and returns with the newly created nested `let` from. Just a small recursive function, nothing special.

## 2021-04-22
### 1.7 Procedures
Implmented the `letrec`, `lambda` form and `lambda` applications of the `letrec` lambdas. For that i extened the `unique-label` function with a label name so it can also be used to generate the `lambda` lables for the `letrec` form. The code itself is pretty much from the `tutorial` templates but with minor modification:
* The `emit-lambda` also needed a `   ret` after the lambda body is emitted. Otherwise it would just execute the code below the function.
* The `emit-app` with it's interal `emit-arguments` need to actually save the evaluated arguments on the stack. Also generated some comments for the argument evaluation in the assembly file. So it's easier to understand the generated assembly file.
* Changed the application grammer from `(app lambda-var args ...)` to `(lambda-var args ...)`

The implmentation itself was realtiv easy only had some minor error. Like extracting the wrong body of the `lambda` in the `letrec` bindings. Didn't actually save the evaluated arguments to the stack for the call. And the `emit-adjust-base` did a `sub` instead of a `addl` and also had the wrong order for the gas assembly.

Other then that i made a small refactoring(s) like introducting `(tagged-list? tag expr)` for checking if the list begins with the tag and used it where appropiate.

The `tests-1.7-req.scm` where for some fixnum calculation stuff. Replaced it with the version from [Nada Amin](https://github.com/namin/inc/).

## 2021-05-01
### 1.8 Iteration via Proper Tail Calls
Instead of the porposed changes with a new `emit-tail-expr` function. I just introduced a `emit-any-expr` that takes a additional `tail` boolean that says if the expression is in tail postion. Then just emit returns with the new `emit-return-if-tail` for `immediate`, `variable` and `primcalls` if they are in tail position. The `emit-if` branches and `let` body generations just call the new `emit-any-expr` with the provided tail info. The `emit-and` and `emit-or` also just call the `emit-any-expr` with the provided `tail` flag.

The `emit-app` works the same as before if it is no tail call. For tail calls it first `emit-arguments` as before, only that it emits them starting with the provided `si` because we don't need to make place for the return address. Then it moves the arguments down to the return address. For that i introduced a new new internal procedure `emit-move-arguments` that `emit-stack-load` the arguments and `emit-stack-save` them based on the delta with is just the `si` (`stack-index`) excluding the return point (1 * `word-size`). The last step is to just jump to the target function.

Also modified `emit-scheme-entry` and indirectly `emit-letrec` that it is always in tail position. The `emit-variable-ref` now also uses the new `emit-stack-load` function.

Overall it was pretty strait forward to implement. The only thing was that it took me some time to warp my head around that i can just move the arguments down to the stack-index (`si`) `0` + `word-size` (the return point need to keep alive). Because the callee's view from a call instrucation is based on a new stack-index on the new base return point and tail calls only will never actually grow the base of the stack because they will just jmp to the code with the newly collapsed stack frame. But there may sill lurke a bug when mixed together, not 100% sure that it's correct.

## 2021-05-03
### 1.9 Heap Allocation

Implemented the proposed context save and restore by copying the code from the tutorial. At first i replaced the `esp` with the `stack-top` before i saved the `heap` into `ebp` which of course can't work. Because by that point it would take the `heap` from a new uninialized stack location. Played around a bit with `ctest.c` and `gcc -m32 --omit-frame-pointer -fno-asynchronous-unwind-tables -S ctest.c` before figuring out what was wrong.

### 1.9.1 Pairs
Adding the code for `cons`, `car` and `cdr` support in the compiler was easy. Only had some typos in the generated assembler like `eax` instead of `%eax` or `mov` instead of `movl` nothing serious. But on the other hand adding the printing support in `C` was a pain in the ass. Always had segfaults because of some small errors. For example i wrote a check function `is_pair` that had a bug in it because it was doing the following `return (x & pair_tag) == pair_tag` instead of `return (x & pair_mask) == pair_tag`. For most tests it runs flawless because the representations of course overlap if only the last tag bit is considered. Only the last test `(cons (let ((x #t)) (let ((y (cons x x))) (cons x y))) (cons (let ((x #f)) (let ((y (cons x x))) (cons y x))) ())` which should return `((#t #t . #t) ((#f . #f) . #f)` ended in a segfault. After flushing after each `printf` it printed `((#t #t ` and then segfaulted. After that i also printed the cdr with `printf("%u", cdr)` and there it was `111` => `1101111` for the `#t` value. Of course that was no pointer like `4090454041` so i looked up my test function and of course saw the obvious bug.

Also before that i did make sure my assembly should be correct. To do so i just warp every `emit-any-expr` with a `# begin: <expr>` and `# end:   <expr>` so following along the generated assembly is now even easier. And read througth the assembly file for the test line by line and verified by hand that it should work.

The c printing part itself is now split up in different print functions that call each other recrusively until the list is processed. For the other expressions it's pretty much the same. Only added a indirection with `print_ptr_line` without the linebreak so i can reuse it in `print_list_content` to actually print the `car` of the pair. Handling the dotted pairs required also a bit of thinking when the ` . ` should be printed or not. But basically the alogrithmen is the following:
* if it is no pair just `print_ptr_line` it
* otherwise print the extracted `car` with `print_ptr_line`
* if the cdr is the empty list abort
* otherwise print ` ` and if the `cdr` is no pair also print `. ` (so we have the dotted pair representation)
* and then print the `cdr` with `print_list_content`

The `print_list_content` is warped in `printf("(")` and `printf(")")` in the `print_ptr_line` so no extra flags for `in list` or something is need. At first i modified the print_ptr funtion with 2 extra flags `new_line` and `in_list` but that was horrible to read and the list printing was still buggy.

## 2021-05-04

### 1.9.1 Pairs
Implemented the special form `begin` because it was used in the tests. Also added implicit begin for `let`, `let*` and `lambda` special forms and finished the pair implementions with the functions `set-car!` and `set-cdr!`. The tests use the `eq?` function so i implemented it too.

Forgot to commit the `tests-1.9.1-req.scm` but now it's in the master. Overall it was all pretty strait forward to implement no stepping stone this time.

## 2021-05-08

### 1.9.2 Vectors
Implemented `make-vector`, `vector?`, `vector-length`, `vector-set!` and `vector-ref`. Also added the new test file `tests-1.9.2-req.scm`.

The implementation was similiar to the Pair implementation but the alignment in `make-vector` did need some love. At first i just used the size parameter which of course don't work for non numbers. So i changed that by `eval-expr` the size and then calculate the 8 byte aligment in assembler for the `ebp` offset. Because we know that it can only differ by `4` bytes it's just a test if the lower 3 bits are 0 if not add 4.
Also my first version of `vector-ref` was buggy, it just returned the size because i forget to add the calculated offset before getting the value out. So basically i always returned the size of the vector and the tests didn't directly cover that simple case so i added some cases for that.

## 2021-05-17
### 1.9.3 Strings
Implemented `make-string`, `string?`, `string-length`, `string-set!` and `string-ref`. And added the new test file `tests-1.9.3-req.scm`.

The string implementation is nearly the same as the vector implementation. The only difference is that each char only require 1 byte instead of the 4 from the vector. So the aligment calculation and setting and receiving chars from the string is a bit different.


# Conclusion
The tutorial was a nice introduction to a simple 32 bit x86 compiler without any optimizations. Also the the development of the c "runtime" and how the c structures are represented in machine code was a nice learing expierence. Also learned a bit about the x86 status registers and some op codes that may come in handy some time later on. The language itself in the tutorial is too simple to be really useable for anything else as for learning but with the other paper `An Incremental Approach to Compiler Construction` which i want to taggle next that may change.

Positive Points:
* Like the simple incremental style of growing the compiler bit by bit
* Each chapter is small enough to pretty much implement it in one go and learn a bit on the way
* Also the chapters itself have a good ordering of complexity. They start as simple as possible and get more involved later on
* The test driven development allowes one to make changes to the compiler with a higher confidence that nothing is broken afterwards

Some minor negative Points:
* From a learing perspective i thing it would have been even smoother to not deal with different tag sizes and just use a 3 or 8 bit tag for all and be done with it
* The structure of the compiler is strait forward but far from modular which is fine for the tuturial but when somebody wanna grow that into a hole compiler it would need some restructuring
