(load "primitives.scm")

(load "tests/tests-driver.scm")
(load "tests/tests-1.9.3-req.scm")
(load "tests/tests-1.9.2-req.scm")
(load "tests/tests-1.9.1-req.scm")
(load "tests/tests-1.8-req.scm")
(load "tests/tests-1.7-req.scm")
(load "tests/tests-1.6-opt.scm")
(load "tests/tests-1.6-req.scm")
(load "tests/tests-1.5-req.scm")
(load "tests/tests-1.4-req.scm")
(load "tests/tests-1.3-req.scm")
(load "tests/tests-1.2-req.scm")
(load "tests/tests-1.1-req.scm")

(define fx-shift        2) ; we shift only the last 2 bits
(define fx-mask      #x03) ; just the last 2 bit with ar '00'
(define fx-tag       #x00) ; the empty tag '00'
(define char-shift      8) ; we shift the hole 8 bits
(define char-mask    #xFF) ; just match the hole 8 bits
(define char-tag     #x0F) ; the tag '00001111'
(define empty-list   #x3F) ; 00111111
(define bool-f       #x2F) ; 00101111
(define bool-t       #x6F) ; 01101111
(define bool-mask    #x3F) ; we match the all lower 6 bits to extract the '101111' pattern. ignoring the actual bool value
(define bool-shift      6) ; bool bit position is at the 7th bit. for shifting left that means we shift it 6 to the left
(define word-size       4) ; bytes
(define object-mask  #x07) ; we use the last 3 bits for pairs, closures, symblos, vectors and strings
(define pair-tag     #x01) ; 001
(define vector-tag   #x05) ; 101
(define string-tag   #x06) ; 110

(define fixnum-bits (- (* word-size 8) fx-shift))

(define fx-lower (- (expt 2 (- fixnum-bits 1))))

(define fx-upper (sub1 (expt 2 (- fixnum-bits 1))))

(define (fixnum? x)
  (and (integer? x) (exact? x) (<= fx-lower x fx-upper)))

(define (immediate? x)
  (or (fixnum? x)
      (boolean? x)
      (null? x)
      (char? x)))

(define (immediate-rep x)
  (cond
   [(fixnum? x) (ash x fx-shift)]
   [(boolean? x) (if x bool-t bool-f)]
   [(null? x) empty-list]
   [(char? x) (+ (ash (char->integer x) char-shift) char-tag)]))

(define (compound? x)
  (or (pair? x)))

(define (compound-rep x)
  (cond
   [(pair? x)
    x]))

(define (emit-function-header function-name)
  (emit "    .text")
  (emit "    .globl ~a" function-name)
  (emit "    .type ~a, @function" function-name)
  (emit "~a:" function-name))

(define (emit-immediate x)
  (emit "    movl $~s, %eax" (immediate-rep x)))


(define (if? expr)
  (and (tagged-list? 'if expr)
       (or (= (length expr) 4)
           (error 'if? (format "malformed if ~s" expr)))))

(define if-test cadr)
(define if-conseq caddr)
(define if-altern cadddr)

(define unique-label
  (let ([count 0])
    (lambda (name)
      (let ([L (format "~a_~s" name count)])
        (set! count (add1 count))
        L))))

(define (emit-label label)
  (emit "~a:" label))

(define (emit-if si env tail expr)
  (let ([alt-label (unique-label "alt")]
        [end-label (unique-label "end")])
    (emit-expr si env (if-test expr))
    (emit "    cmp $~s, %al" bool-f)
    (emit "    je ~a" alt-label)
    (emit-any-expr si env tail (if-conseq expr))
    (emit "    jmp ~a" end-label)
    (emit-label alt-label)
    (emit-any-expr si env tail (if-altern expr))
    (emit-label end-label)))

(define (and? expr)
  (tagged-list? 'and expr))

;; it's a hack. should be replaced with a `and` macro. but the tutorial won't go so far!
(define (emit-and si env tail expr)
  (cond
   [(= (length expr) 1) (emit-any-expr si env tail #t)]
   [(= (length expr) 2) (emit-any-expr si env tail (cadr expr))]
   [(> (length expr) 2) (emit-any-expr si env tail `(if ,(cadr expr) (and ,@(cddr expr)) #f))]))

(define (or? expr)
  (tagged-list? 'or expr))

(define (emit-or si env tail expr)
  (cond
   [(= (length expr) 1) (emit-any-expr si env tail #f)]
   [(= (length expr) 2) (emit-any-expr si env tail (cadr expr))]
   ;; only evalute the first expression once by introducing a let binding for it and return the evaluated var if it is true.
   ;; That way we can also place functions with side effects into
   [(> (length expr) 2) (emit-any-expr si env tail
                                   (let ((unique-var-label (string->symbol (unique-label "or"))))
                                     `(let ((,unique-var-label ,(cadr expr)))
                                        (if ,unique-var-label ,unique-var-label (or ,@(cddr expr))))))]))

(define (emit-stack-save si)
  (emit "    movl %eax, ~s(%esp)" si))

(define (emit-stack-load si)
  (emit "    movl ~s(%esp), %eax" si))

(define (next-stack-index si)
  (- si word-size))

(define (let? expr)
  (and (tagged-list? 'let expr) (>= (length expr) 3)))

(define let-bindings    cadr)
(define let-body-all    cddr)
(define let-body-first caddr)

(define (emit-let si env tail expr)
  (define (process-let bindings si new-env)
    (cond
     [(null? bindings)
      (emit-implicit-begin si new-env tail (let-body-all expr))]
     [else
      (let ([binding (car bindings)])
        (emit-expr si env (cadr binding))
        (emit-stack-save si)
        (process-let (cdr bindings)
                     (next-stack-index si)
                     (extend-env (car binding) si new-env)))]))
  (process-let (let-bindings expr) si env))

(define (let*? expr)
  (and (tagged-list? 'let* expr) (>= (length expr) 3)))

;; todo: remove this special handling as soon as the language itself support macros or something similiar
(define (emit-let* si env tail expr)
  (define (transform-let*-to-let bindings let-body)
    (if (null? bindings)
        let-body
        `(let (,(car bindings))
           ,(transform-let*-to-let (cdr bindings) let-body))))
  (emit-let si env tail (transform-let*-to-let (let-bindings expr) (let-body-first expr))))

(define (emit-variable-ref env var)
  (cond
   [(lookup var env)
    (emit-stack-load (cdr (lookup var env)))]
   [else (error 'emit-variable-ref (format "Non bound var: ~s" var))]))

(define variable? symbol?)
(define lookup assoc)
(define (extend-env var si env)
  (cons (cons var si) env))

(define (make-initial-env vars values)
  (map (lambda (var value) (cons var value)) vars values))

(define (tagged-list? tag expr)
  (and (pair? expr) (equal? tag (car expr))))

(define (letrec? expr)
  (and (tagged-list? 'letrec expr) (= (length expr) 3)))

(define (emit-letrec expr)
  (let* ([bindings (let-bindings expr)]
         [lvars (map car bindings)]
         [lambdas (map cadr bindings)]
         [labels (map unique-label lvars)]
         [env (make-initial-env lvars labels)])
    (for-each (emit-lambda env) lambdas labels)
    (emit-scheme-entry env (let-body-first expr)))) ; we only allow one expression in the letrec body for now

(define (lambda? expr)
  (tagged-list? 'lambda expr))

(define lambda-formals cadr)
(define lambda-body    cddr)

(define (emit-lambda env)
  (lambda (expr label)
    (emit-function-header label)
    (let ([formals (lambda-formals expr)]
          [body    (lambda-body expr)])
      (let f ([formals formals] [si (- word-size)] [env env])
        (cond
         [(null? formals)
          (emit-implicit-begin si env #t body)]
         [else
          (f (cdr formals)
             (next-stack-index si)
             (extend-env (car formals) si env))])))))

(define call-target car)
(define call-args cdr)

(define (app? expr env)
  (and (list? expr) (not (null? expr)) (lookup (call-target expr) env)))

(define (emit-adjust-base si)
  (when (or (< si 0) (> si 0)) ; don't generate non ajusting base code
    (emit "    addl $~s, %esp" si)))

(define (emit-call function-mapping)
  (emit "    call ~a" (cdr function-mapping)))

(define (emit-jmp function-mapping)
  (emit "    jmp ~a" (cdr function-mapping)))

(define (emit-app si env tail expr)
  (define (emit-arguments si args)
    (unless (null? args)
      (emit-expr si env (car args)) ; first evaluate expression
      (emit-stack-save si)          ; then save it on stack for the call
      (emit-arguments (next-stack-index si) (cdr args))))
  (define (emit-move-arguments si delta args)
    (when (and (> delta 0) (> (length args) 0))
      (emit-stack-load si)
      (emit-stack-save (+ si delta))
      (emit-move-arguments (- si word-size) delta (cdr args))))
  (cond
   [tail
    (emit-arguments si (call-args expr))
    (emit-move-arguments si (- (- si) word-size) (call-args expr))
    (emit-jmp (lookup (call-target expr) env))]
   [else
    (emit-arguments (next-stack-index si) (call-args expr))
    (emit-adjust-base (+ si word-size))
    (emit-call (lookup (call-target expr) env))
    (emit-adjust-base (- (+ si word-size)))]))

(define (begin? expr)
  (tagged-list? 'begin expr))

(define (emit-begin si env tail expr)
  (emit-implicit-begin si env tail (cdr expr)))

(define (emit-implicit-begin si env tail expr)
  (cond
   [(= (length expr) 1)
    (emit-any-expr si env tail (car expr))]
   [else
    (emit-expr si env (car expr))
    (emit-implicit-begin si env tail (cdr expr))]))

(define (emit-expr si env expr)
  (emit-any-expr si env #f expr))

(define (emit-return-if-tail tail)
  (when tail
    (emit "    ret")))

(define (emit-any-expr si env tail expr)
  (emit "# begin: ~s" expr)
  (cond
   [(immediate? expr)  (emit-immediate expr) (emit-return-if-tail tail)]
   [(variable? expr)   (emit-variable-ref env expr) (emit-return-if-tail tail)]
   [(if? expr)         (emit-if si env tail expr)]
   [(and? expr)        (emit-and si env tail expr)]
   [(or? expr)         (emit-or si env tail expr)]
   [(begin? expr)      (emit-begin si env tail expr)]
   [(let? expr)        (emit-let si env tail expr)]
   [(let*? expr)       (emit-let* si env tail expr)]
   [(app? expr env)    (emit-app si env tail expr) (emit-return-if-tail tail)]
   [(primcall? expr)   (emit-primcall si env expr) (emit-return-if-tail tail)] ; see: primitives.scm
   [else (error 'emit-expr (format "unkown expr ~s" expr))])
   (emit "# end:   ~s" expr))

(define (emit-scheme-entry env expr)
  (emit-function-header "L_scheme_entry")
  (emit-any-expr (next-stack-index 0) env #t expr))

(define (compile-program expr)
  (if (letrec? expr)
      (emit-letrec expr)
      (emit-scheme-entry '() expr))
  (emit-function-header "scheme_entry")
  (emit "    movl 4(%esp), %ecx")   ; move context adress to ecx
  (emit "    movl %ebx, 4(%ecx)")   ; store ebx
  (emit "    movl %esi, 16(%ecx)")  ; store esi
  (emit "    movl %edi, 20(%ecx)")  ; store edi
  (emit "    movl %ebp, 24(%ecx)")  ; store ebp
  (emit "    movl %esp, 28(%ecx)")  ; store esp
  (emit "    movl 12(%esp), %ebp")  ; move heap adress to ebp
  (emit "    movl 8(%esp), %esp")   ; at least move stack-top adress to esp
  (emit "    call L_scheme_entry")
  (emit "    movl 4(%ecx), %ebx")   ; restore ebx
  (emit "    movl 16(%ecx), %esi")  ; restore esi
  (emit "    movl 20(%ecx), %edi")  ; restore edi
  (emit "    movl 24(%ecx), %ebp")  ; restore ebp
  (emit "    movl 28(%ecx), %esp")  ; restore esp
  (emit "    ret"))
