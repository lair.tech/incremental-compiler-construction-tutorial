
;; -------------------------------------------------------------------
;; Functions for creating and handling primitive unary/binary
;; functions in the compiler
;; -------------------------------------------------------------------

(define-syntax define-primitive
  (syntax-rules ()
    [(_ (prim-name si env arg arg* ...) b b* ...)
     (begin
       (putprop 'prim-name '*is-prim*   #t)
       (putprop 'prim-name '*arg-count* (length '(arg arg* ...)))
       (putprop 'prim-name '*emitter*   (lambda (si env arg arg* ...)
                                          b b* ...)))]))

(define (primitive? x)
  (and (symbol? x) (getprop x '*is-prim*)))

(define (primitive-emitter x)
  (or (getprop x '*emitter*) (error 'primitive-emitter (format "unkown primitive: ~s" x))))

(define (primcall? expr)
  (and (pair? expr) (primitive? (car expr))))

(define (emit-primcall si env expr)
  (let ([prim (car expr)]
        [args (cdr expr)])
    (check-primcall-args prim args)
    (apply (primitive-emitter prim) si env args)))

(define (check-primcall-args prim args)
  (let ([args-length (length args)]
        [prim-length (getprop prim '*arg-count*)])
    (or (= args-length prim-length)
        (error 'check-primcall-args
               (format "wrong number of arguments for ~s. ~s instead of ~s" prim args-length prim-length)))))

;; helper functions for returning boolean values
(define (emit-cmp-bool to setx)
  (emit "    cmp $~s, %eax" to)
  (emit-setx-bool setx))

(define (emit-setx-bool setx)
  (emit "    ~s %al" setx)
  (emit "    movzbl %al, %eax")
  (emit "    sal $~s, %al" bool-shift)
  (emit "    or $~s, %al" bool-f))

;; -------------------------------------------------------------------
;; Unary Primitives
;; -------------------------------------------------------------------
(define (emit-unary-operator si env operator arg constant)
  (emit-expr si env arg)
  (emit "    ~s $~s, %eax" operator constant))

(define-primitive (fxadd1 si env arg)
  (emit-unary-operator si env 'addl arg (immediate-rep 1)))

(define-primitive (fxsub1 si env arg)
  (emit-unary-operator si env 'subl arg (immediate-rep 1)))

(define-primitive (fixnum->char si env arg)
  (emit-unary-operator si env 'shll arg (- char-shift fx-shift)) ; shift 6 to the left and then add the char tag
  (emit "    orl $~s, %eax" char-tag))

(define-primitive (char->fixnum si env arg)
  (emit-unary-operator si env 'shrl arg (- char-shift fx-shift)))

(define-primitive (fixnum? si env arg)
  (emit-unary-operator si env 'and arg fx-mask)
  (emit-cmp-bool fx-tag 'sete))

(define-primitive (fxzero? si env arg)
  (emit-expr si env arg)
  (emit-cmp-bool 0 'sete))

(define-primitive (null? si env arg)
  (emit-expr si env arg)
  (emit-cmp-bool empty-list 'sete))

(define-primitive (boolean? si env arg)
  (emit-unary-operator si env 'and arg bool-mask)
  (emit-cmp-bool bool-f 'sete))

(define-primitive (char? si env arg)
  (emit-unary-operator si env 'and arg char-mask)
  (emit-cmp-bool char-tag 'sete))

(define-primitive (not si env arg)
  (emit-expr si env arg)
  (emit-cmp-bool bool-f 'sete))

(define-primitive (fxlognot si env arg)
  (emit-unary-operator si env 'shrl arg fx-shift)
  (emit "    not %eax") ; after right shift, negate and shift back
  (emit "    shll $~s, %eax" fx-shift))

(define-primitive (pair? si env arg)
  (emit-unary-operator si env 'and arg object-mask)
  (emit-cmp-bool pair-tag 'sete))

(define-primitive (car si env arg)
  (emit-expr si env arg)
  (emit "    movl -1(%eax), %eax"))

(define-primitive (cdr si env arg)
  (emit-expr si env arg)
  (emit "    movl 3(%eax), %eax"))

(define-primitive (vector? si env arg)
  (emit-unary-operator si env 'and arg object-mask)
  (emit-cmp-bool vector-tag 'sete))

(define-primitive (make-vector si env size); init)
  (let ([end-label  (unique-label "end")])
    (emit-expr si env size)
    (emit "    movl %eax, (%ebp)")
    (emit "    movl %eax, %edx")
    (emit "    movl %ebp, %eax")
    (emit "    addl $~s, %eax" vector-tag)
    (emit "    shrl $~s, %edx" fx-shift)
    (emit "    imul $~s, %edx" word-size)
    (emit "    addl $~s, %edx" word-size)
    (emit "    addl %edx, %ebp")
    (emit "    andl $~s, %edx" object-mask)
    (emit "    cmp $0, %edx")
    (emit "    je ~a" end-label)
    (emit "    add $~s, %ebp" word-size)  ; make sure the allocation pointer is 8 byte aligned
    (emit "~a:" end-label)))

(define-primitive (vector-length si env vector)
  (emit-expr si env vector)
  (emit "    movl -~s(%eax), %eax" vector-tag))

(define-primitive (string? si env arg)
  (emit-unary-operator si env 'and arg object-mask)
  (emit-cmp-bool string-tag 'sete))

(define-primitive (make-string si env size); init)
  (let ([end-label  (unique-label "end")])
    (emit-expr si env size)
    (emit "    movl %eax, (%ebp)")
    (emit "    movl %eax, %edx")
    (emit "    movl %ebp, %eax")
    (emit "    addl $~s, %eax" string-tag)
    (emit "    shrl $~s, %edx" fx-shift)
    (emit "    addl $~s, %edx" word-size)
    (emit "    addl %edx, %ebp")
    (emit "    andl $~s, %edx" object-mask)
    (emit "    cmp $0, %edx")
    (emit "    je ~a" end-label)
    (emit "    subl $8, %edx")
    ; (emit "    negl %edx") we just substract the negative number from epb so it will be 8 byte aligned
    (emit "    subl %edx, %ebp")
    (emit "~a:" end-label)))

(define-primitive (string-length si env vector)
  (emit-expr si env vector)
  (emit "    movl -~s(%eax), %eax" string-tag))
;; -------------------------------------------------------------------
;; Binary Primitives
;; -------------------------------------------------------------------

(define (emit-binary-operator si env operator arg1 arg2 . communtativ)
  (cond
   [(immediate? arg1)
    (emit-expr si env arg2)
    (emit "    ~s $~s, %eax" operator (immediate-rep arg1))]
   [(and (immediate? arg2) (or (null? communtativ) (car communtativ)))
    (emit-expr si env arg1)
    (emit "    ~s $~s, %eax" operator (immediate-rep arg2))]
   [else
    (emit-expr si env arg1)
    (emit "    movl %eax, ~s(%esp)" si)
    (emit-expr (next-stack-index si) env arg2)
    (emit "    ~s ~s(%esp), %eax" operator si)]))

(define-primitive (cons si env car-arg cdr-arg)
  (emit-expr si env cdr-arg)
  (emit-stack-save si)   ; temp save evaluated cdr
  (emit-expr (next-stack-index si) env car-arg)
  (emit "    movl %eax, (%ebp)")   ; save car in actual free ebp space
  (emit-stack-load si)  ; restore evaluated cdr
  (emit "    movl %eax, 4(%ebp)")  ; save cdr in the next free ebp space
  (emit "    movl %ebp, %eax")     ; place pair pointer into eax
  (emit "    addl $~s, %eax" pair-tag) ; add pair tag
  (emit "    addl $~s, %ebp" (* 2 word-size))) ; move the ebp to the next free space (2 * word-size)

(define-primitive (set-car! si env con value)
  (emit-expr si env con)
  (emit "    movl %eax, %edx")
  (emit-expr si env value)
  (emit "    movl %eax, -1(%edx)")
  (emit "    movl %edx, %eax"))

(define-primitive (set-cdr! si env con value)
  (emit-expr si env con)
  (emit "    movl %eax, %edx")
  (emit-expr si env value)
  (emit "    movl %eax, 3(%edx)")
  (emit "    movl %edx, %eax"))

(define-primitive (vector-set! si env vector index value)
  (emit-expr si env vector)
  (emit "    movl %eax, %edx")
  (emit-expr si env index)
  (emit "    shrl $~s, %eax" fx-shift)
  (emit "    imul $4, %eax")
  (emit "    addl $4, %eax")
  (emit "    addl %eax, %edx")
  (emit-expr si env value)
  (emit "    movl %eax, -~s(%edx)" vector-tag))

(define-primitive (vector-ref si env vector index)
  (emit-expr si env vector)
  (emit "    movl %eax, %edx")
  (emit-expr si env index)
  (emit "    shrl $~s, %eax" fx-shift)
  (emit "    imul $4, %eax")
  (emit "    addl $4, %eax")
  (emit "    addl %eax, %edx")
  (emit "    movl -~s(%edx), %eax" vector-tag))

(define-primitive (string-set! si env string index char)
  (emit-expr si env string)
  (emit "    movl %eax, %edx")
  (emit-expr si env index)
  (emit "    shrl $~s, %eax" fx-shift)
  (emit "    addl $4, %eax")
  (emit "    addl %eax, %edx")
  (emit-expr si env char)
  (emit "    shrl $~s, %eax" char-shift)
  (emit "    movb %al, -~s(%edx)" string-tag))

(define-primitive (string-ref si env string index)
  (emit-expr si env string)
  (emit "    movl %eax, %edx")
  (emit-expr si env index)
  (emit "    shrl $~s, %eax" fx-shift)
  (emit "    addl $4, %eax")
  (emit "    addl %eax, %edx")
  (emit "    movzbl -~s(%edx), %eax" string-tag)
  (emit "    shll $~s, %eax" char-shift)
  (emit "    add $~s, %eax" char-tag))

(define-primitive (fx+ si env arg1 arg2)
  (emit-binary-operator si env 'addl arg1 arg2))

(define-primitive (fx- si env arg1 arg2)
  (emit-binary-operator si env 'sub arg2 arg1 #f))

(define-primitive (fx* si env arg1 arg2)
  (emit-expr si env arg1)
  (emit "    shrl $~s, %eax" fx-shift) ; shift right before multiplication
  (emit "    movl %eax, ~s(%esp)" si)
  (emit-expr (next-stack-index si) env arg2)
  (emit "    shrl $~s, %eax" fx-shift) ; shift right before multiplication
  (emit "    imul ~s(%esp), %eax" si)
  (emit "    shll $~s, %eax" fx-shift)) ; shift left after multiplication (now we have scheme value again)

(define-primitive (fxlogor si env arg1 arg2)
  (emit-binary-operator si env 'orl arg1 arg2))

(define-primitive (fxlogand si env arg1 arg2)
  (emit-binary-operator si env 'andl arg1 arg2))

;; -------------------------------------------------------------------
;; Binary Comparsion Primitives
;; -------------------------------------------------------------------

(define (emit-binary-compare si env setx-base setx-flip arg1 arg2)
  (cond
   [(immediate? arg1)
    (emit-binary-operator si env 'cmp arg1 arg2)
    (emit-setx-bool setx-flip)]
   [else
    (emit-binary-operator si env 'cmp arg2 arg1)
    (emit-setx-bool setx-base)]))

;; fixnum comparsion
(define-primitive (fx= si env arg1 arg2)
  (emit-binary-compare si env 'sete 'sete arg1 arg2))

(define-primitive (fx< si env arg1 arg2)
  (emit-binary-compare si env 'setl 'setg arg1 arg2))

(define-primitive (fx<= si env arg1 arg2)
  (emit-binary-compare si env 'setle 'setge arg1 arg2))

(define-primitive (fx> si env arg1 arg2)
  (emit-binary-compare si env 'setg 'setl arg1 arg2))

(define-primitive (fx>= si env arg1 arg2)
  (emit-binary-compare si env 'setge 'setle arg1 arg2))

;; char comparsions
(define-primitive (char= si env arg1 arg2)
  (emit-binary-compare si env 'sete 'sete arg1 arg2))

(define-primitive (char< si env arg1 arg2)
  (emit-binary-compare si env 'setl 'setg arg1 arg2))

(define-primitive (char<= si env arg1 arg2)
  (emit-binary-compare si env 'setle 'setge arg1 arg2))

(define-primitive (char> si env arg1 arg2)
  (emit-binary-compare si env 'setg 'setl arg1 arg2))

(define-primitive (char>= si env arg1 arg2)
  (emit-binary-compare si env 'setge 'setle arg1 arg2))

;; equal comparsions
(define-primitive (eq? si env arg1 arg2)
  (emit-binary-compare si env 'sete 'sete arg1 arg2))
